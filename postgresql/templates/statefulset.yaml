apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "postgresql-helm.fullname" . }}
  labels:
    {{- include "postgresql-helm.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  serviceName: {{ include "postgresql-helm.fullname" . }}
  selector:
    matchLabels:
      {{- include "postgresql-helm.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "postgresql-helm.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      volumes:
        {{ if .Values.tls.enabled -}}
        - name: certs
          secret:
            defaultMode: 0640
            secretName: {{ .Values.tls.certificatesSecret }}
        {{- end }}
        - configMap:
            defaultMode: 0640
            name: {{ include "postgresql-helm.fullname" . }}
          name: config
        - name: data
          {{- .Values.volume | nindent 10 }}
        - emptyDir:
            medium: Memory
          name: shm
      containers:
        - name: postgresql
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default (printf "%s-alpine" .Chart.AppVersion) }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: "postgres-password"
                  name: {{ include "postgresql-helm.fullname" . }}
          ports:
            - name: sql
              containerPort: 5432
              protocol: TCP
          startupProbe:
            exec:
              command:
                - sh
                - -c
                - pg_isready --host localhost && psql -c "ALTER USER postgres WITH PASSWORD '${POSTGRES_PASSWORD}';"
          livenessProbe:
            exec:
              command:
                - sh
                - -c
                - exec pg_isready --host localhost
          readinessProbe:
            exec:
              command:
                - sh
                - -c
                - exec pg_isready --host localhost
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - mountPath: "/var/lib/postgresql/data"
              name: data
              subPath: "{{ .Chart.AppVersion }}/data"
            - mountPath: "/configs"
              name: config
            - mountPath: "/dev/shm"
              name: shm
            {{ if .Values.tls.enabled -}}
            - mountPath: "/certs/tls.crt"
              name: certs
              subPath: tls.crt
            - mountPath: "/certs/tls.key"
              name: certs
              subPath: tls.key
            {{- end }}
        {{ if .Values.exporter.enabled }}
        - name: exporter
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.exporter.repository }}:{{ .Values.exporter.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: PG_EXPORTER_AUTO_DISCOVER_DATABASES
              value: "true"
            - name: DATA_SOURCE_URI
              value: "localhost"
            - name: DATA_SOURCE_USER
              value: "postgres"
            - name: DATA_SOURCE_PASS
              valueFrom:
                secretKeyRef:
                  key: "postgres-password"
                  name: {{ include "postgresql-helm.fullname" . }}
          ports:
            - name: metrics
              containerPort: 9187
              protocol: TCP
          startupProbe:
            httpGet:
              port: metrics
              path: /metrics
          livenessProbe:
            httpGet:
              port: metrics
              path: /metrics
          readinessProbe:
            httpGet:
              port: metrics
              path: /metrics
          resources:
            {{- toYaml .Values.exporter.resources | nindent 12 }}
        {{ end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
