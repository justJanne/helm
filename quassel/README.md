# Helm Chart for Quassel Core

## TL;DR

```console
$ helm repo add justjanne https://helm.k8r.eu/
$ helm install -f values.yaml quassel-core --set fullnameOverride=quassel-core justjanne/quassel
```

## Introduction

This chart installs a [Quassel Core] deployment on a [Kubernetes] cluster using 
the [Helm] package manager.

It supports the following architectures: `x86-64`, `aarch64`, `armhf`.  
For non-x86_64 deployments, use `--set image.tag=v0.14.0-aarch64` (or 
`v0.14.0-armhf` accordingly)

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm install my-release justjanne/quassel-helm
```

The command deploys Quassel Core on the Kubernetes cluster in the default 
configuration. See [values.yaml] for available configuration options.

## Exposing the Chart

You’ll have to configure your ingress controller, load balancer, or an
equivalent resource to expose the quassel service from your cluster.

With the [NGINX Ingress Controller], you’d  add the following line to your 
[TCP service map]:

```diff
apiVersion: v1
kind: ConfigMap
metadata:
  name: tcp-services
  namespace: ingress-nginx
data:
  9000: "default/example-go:8080"
+   4242: "default/quassel-helm-my-release:4242"
```

If using the proxy protocol, you’d add the "::PROXY" suffix.

If using a different ingress controller, load balancer, etc., please follow
their documentation for how to expose TCP services.

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and 
deletes the release.

---

[Quassel Core]: https://quassel-irc.org/
[Kubernetes]: https://kubernetes.io
[Helm]: https://helm.sh
[values.yaml]: https://git.kuschku.de/justJanne/quassel-helm/-/blob/main/values.yaml
[NGINX Ingress Controller]: https://kubernetes.github.io/ingress-nginx/
[TCP service map]: https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/#exposing-tcp-and-udp-services
