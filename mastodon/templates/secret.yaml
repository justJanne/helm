apiVersion: v1
kind: Secret
metadata:
  name: {{ include "mastodon-helm.fullname" . }}
  labels:
    {{- include "mastodon-helm.labels" . | nindent 4 }}
stringData:
  # This is a sample configuration file. You can generate your configuration
  # with the `rake mastodon:setup` interactive setup wizard, but to customize
  # your setup even further, you'll need to edit it manually. This sample does
  # not demonstrate all available configuration options. Please look at
  # https://docs.joinmastodon.org/admin/config/ for the full documentation.

  # Note that this file accepts slightly different syntax depending on whether
  # you are using `docker-compose` or not. In particular, if you use
  # `docker-compose`, the value of each declared variable will be taken verbatim,
  # including surrounding quotes.
  # See: https://github.com/mastodon/mastodon/issues/16895

  # Mode
  RAILS_ENV: production
  NODE_ENV: production

  # Federation and access
  # ----------
  # This identifies your server and cannot be changed safely later
  # ----------
  LOCAL_DOMAIN: "{{ .Values.config.domainHandle }}"
  WEB_DOMAIN: "{{ .Values.config.domainWeb }}"
  TRUSTED_PROXY_IP: "{{ .Values.config.trustedSubnet }}"

  # Redis
  # -----
  REDIS_URL: "redis://:{{ .Values.redis.password }}@{{ .Values.redis.hostname }}:{{ .Values.redis.port }}/{{ .Values.redis.database }}"

  # PostgreSQL
  # ----------
  DB_HOST: "{{ .Values.postgres.hostname }}"
  DB_PORT: "{{ .Values.postgres.port}}"
  DB_USER: "{{ .Values.postgres.username }}"
  DB_NAME: "{{ .Values.postgres.database }}"
  DB_PASS: "{{ .Values.postgres.password }}"

  # Elasticsearch (optional)
  # ------------------------
  ES_ENABLED: "{{ .Values.elasticsearch.enabled }}"
  ES_HOST: "{{ .Values.elasticsearch.hostname }}"
  ES_PORT: "{{ .Values.elasticsearch.port }}"
  ES_PREFIX: "{{ .Values.elasticsearch.database }}"
  # Authentication for ES (optional)
  ES_USER: "{{ .Values.elasticsearch.username }}"
  ES_PASS: "{{ .Values.elasticsearch.password }}"

  # Secrets
  # -------
  # Make sure to use `rake secret` to generate secrets
  # -------
  SECRET_KEY_BASE: "{{ .Values.keys.secretKeyBase }}"
  OTP_SECRET: "{{ .Values.keys.otpSecret }}"

  # Web Push
  # --------
  # Generate with `rake mastodon:webpush:generate_vapid_key`
  # --------
  VAPID_PRIVATE_KEY: "{{ .Values.keys.vapidPrivate }}"
  VAPID_PUBLIC_KEY: "{{ .Values.keys.vapidPublic }}"

  # Sending mail
  # ------------
  SMTP_SERVER: "{{ .Values.smtp.hostname }}"
  SMTP_PORT: "{{ .Values.smtp.port }}"
  SMTP_TLS: "{{ .Values.smtp.tls }}"
  SMTP_ENABLE_STARTTLS: "{{ .Values.smtp.startTls }}"
  SMTP_LOGIN: "{{ .Values.smtp.username }}"
  SMTP_PASSWORD: "{{ .Values.smtp.password }}"
  SMTP_FROM_ADDRESS: "{{ .Values.smtp.from }}"

  # File storage (optional)
  # -----------------------
  S3_ENABLED: "{{ .Values.s3.enabled }}"
  S3_PROTOCOL: "https"
  S3_REGION: "us-east-1"
  S3_HOSTNAME: "{{ .Values.s3.hostname }}"
  S3_ENDPOINT: "https://{{ .Values.s3.hostname }}"
  S3_BUCKET: "{{ .Values.s3.bucket }}"
  AWS_ACCESS_KEY_ID: "{{ .Values.s3.accessKey }}"
  AWS_SECRET_ACCESS_KEY: "{{ .Values.s3.secretKey }}"
  #S3_ALIAS_HOST: "{{ .Values.s3.aliasHost }}"

  # IP and session retention
  # -----------------------
  # Make sure to modify the scheduling of ip_cleanup_scheduler in config/sidekiq.yml
  # to be less than daily if you lower IP_RETENTION_PERIOD below two days (172800).
  # -----------------------
  IP_RETENTION_PERIOD: "31556952"
  SESSION_RETENTION_PERIOD: "31556952"

